package com.ndungx.demoautosuggestusingajax.country;

import java.io.Serializable;

/*
 * @author NDungx
 */
public class CountryDTO implements Serializable {

    String value, data;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
