package com.ndungx.demoautosuggestusingajax.country;

import com.ndungx.demoautosuggestusingajax.utils.DBHelpers;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.NamingException;

/*
 * @author NDungx
 */
public class CountryDAO implements Serializable {

    public static ArrayList<CountryDTO> getCountryName(String id)
            throws SQLException, NamingException {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        ArrayList<CountryDTO> data = new ArrayList<>();
        
        try {
            // fetch a connection
            connection = DBHelpers.makeConnection();
            if (connection != null) {
                ps = connection.prepareStatement("select [country_code], [country_name] "
                        + "from dbo.[countries] "
                        + "where [country_name] like ?");
                ps.setString(1, "%" + id + "%");

                resultSet = ps.executeQuery();
                while (resultSet.next()) {
                    CountryDTO c = new CountryDTO();
                    c.setData(resultSet.getString(1));
                    c.setValue(resultSet.getString(2));
                    data.add(c);
                }
            }
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return data;
    }
}
